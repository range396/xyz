<?php

namespace App\Console\Commands;


use Exception;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem as File;
use Illuminate\Support\Str;

class AddRepository extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repo {repository}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new repository class';

    /**
     *
     *
     * @var File $files
     */
    protected $files;

    /**
     *
     *
     * @var Str $strings
     */
    protected $strings;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(File $file, Str $string)
    {

        parent::__construct();

        $this->files = $file;
        $this->strings = $string;

    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() {

        $name = $this->argument("repository");
        $this->repoDir();

        if (empty($name)) {
            $this->error("Class name is empty: php artisan make:repo { className } ");
            return 0;
        }

        if ($this->files->exists(app_path("Repositories\\{$name}Repository.php"))) {
            $this->error("Repository name already exists, make with another name.");
            return 0;
        }

        $this->repository($name);
        $this->info("Repository created successfuly! \n ******************************");
        $this->call('optimize:clear');
        return 0;
    }

    /**
     *
     *
     * @return void
     */
    protected function repoDir(): void {

        $path = app_path("Repositories");
        if (!$this->files->isDirectory($path)) {
            $this->files->makeDirectory($path, $mode = 755, false, false);
        }

    }

    /**
     *
     *
     * @param string $argument
     * @return void
     */
    protected function repository(string $argument): void {

        $service = app_path("Repositories\\" . "{$argument}" . "Repository.php");
        if (!$this->files->isFile($service)) {
            $content = $this->buildClass($argument);
            $this->files->append($service, $content);
        }

    }

    /**
     *
     * @param string $name
     * @return string
     */
    protected function buildClass(string $name): string {
        $stub = $this->stub("repository");
        return $this->repoNamespace($stub)->repoName($name, $stub);
    }

    /**
     *
     *
     * @param string $type
     * @return string|int
     */
    protected function stub(string $type) {
        $path = getcwd() . "\\resources\stubs\\{$type}.stub";
        if ($this->files->isFile($path)) {
            return $this->files->get($path);
        }
        return 0;
    }

    /**
     *
     *
     * @param string $name
     * @param string $stub
     * @return string;
     */
    protected function repoName(string $name, string $stub): string {
        try {
            return $this->strings->replaceArray("{{ class }}", ["{$name}Repository"], $stub);
        } catch (Exception $e) {
            $this->error("Something went wrong! Message: {$e->getMessage()}");
        }
    }

    /**
     *
     *
     * @param string $namespace
     * @return $this|void
     */
    protected function repoNamespace(&$stub) {
        try {
            $path = app_path("Repositories");
            $namespace = '';
            if ($this->files->isDirectory($path)) {
                $namespace = "App\Repositories";
            }
            $stub = $this->strings->replaceArray("{{ currentNamespace }}", ["{$namespace}"], $stub);
            return $this;
        } catch (Exception $e) {
            $this->error("Something went wrong! Message: {$e->getMessage()}.\n
             Seams like stub template not exists.");
        }
    }
}
