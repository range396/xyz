<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem as File;
use Illuminate\Support\Str;
use Exception;



class AddService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:service {service}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new service class';

    /**
     *
     *
     * @var File $files
     */
    protected $files;

    /**
     *
     *
     * @var Str $strings
     */
    protected $strings;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(File $file, Str $string) {

        parent::__construct();

        $this->files = $file;
        $this->strings = $string;

    }

    /**
     *
     *
     * @return void
     */
    protected function serviceDir(): void {

        $services_path = app_path("Services");
        if (!$this->files->isDirectory($services_path)) {
            $this->files->makeDirectory($services_path, $mode = 755, false, false);
        }

    }

    /**
     *
     *
     * @param string $argument
     * @return void
     */
    protected function service(string $argument): void {

        $service = app_path("Services\\" . "{$argument}" . "Service.php");
        if (!$this->files->isFile($service)) {
            $content = $this->buildClass($argument);
            $this->files->append($service, $content);
        }

    }

    /**
     *
     * @param string $name
     * @return string
     */
    protected function buildClass(string $name): string {
        $stub = $this->stub("services");
        return $this->serviceNamespace($stub)->serviceName($name,$stub);
    }

    /**
     *
     *
     * @param string $name
     * @param string $stub
     * @return string;
     */
    protected function serviceName(string $name, string $stub): string {
        try {
           return $this->strings->replaceArray("{{ class }}", ["{$name}Service"], $stub);
        } catch (Exception $e) {
           $this->error("Something went wrong! Message: {$e->getMessage()}");
        }
    }

    /**
     *
     *
     * @param string $type
     * @return string|int
     */
    protected function stub(string $type) {
        $path = getcwd() . "\\resources\stubs\\{$type}.stub";
        if ($this->files->isFile($path)) {
            return $this->files->get($path);
        }
        return 0;
    }

    /**
     *
     *
     * @param string $namespace
     * @return $this|void
     */
    protected function serviceNamespace(&$stub) {
        try {
            $service_path = app_path("Services");
            $service_namespace = '';
            if ($this->files->isDirectory($service_path)) {
                $service_namespace = "App\Services";
            }
            $stub = $this->strings->replaceArray("{{ currentNamespace }}", ["{$service_namespace}"], $stub);
            return $this;
        } catch (Exception $e) {
            $this->error("Something went wrong! Message: {$e->getMessage()}.\n
             Seams like stub template not exists.");
        }
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() {

        $name = $this->argument("service");
        $this->serviceDir();

        if(empty($name)) {
            $this->error("Class name is empty: php artisan make:service { className } ");
            return 0;
        }

        if($this->files->exists(app_path("Services\\{$name}Service.php"))) {
            $this->error("Service name already exists, make with another name.");
            return 0;
        }

        $this->service($name);
        $this->info("Service created successfuly! \n ******************************");
        $this->call('optimize:clear');

        return 0;
    }


}
