<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Models\Translate;

class TranslateRepository  {


    /**
     *
     * @var Translate
     */
    private $translate;

    /**
     *
     * @param Translate $translate
     */
    public function __construct(Translate $translate) {
        $this->translate = $translate;
    }

    /**
     *
     * @param string $target
     * @return bool
     */
    public function store(array $target) {
       $target["translation"] = html_entity_decode(preg_replace("/U\+([0-9A-F]{4})/", "&#x\\1;", $target["translation"]), ENT_NOQUOTES, 'UTF-8');
       $this->translate->success = $target["success"];
       $this->translate->error = $target["error"];
       $this->translate->translation = $target["translation"];
       return $this->translate->save();
    }


    /**
     *
     * @param string $text
     * @return string
     */
    protected function getHyText(string $text): string
    {
        //  Some Issue with post req. guzzle and curl lib, not understandable, maybe API is block the request
        /*try {


            $headers = [
                "accept-encoding: application/json",
                "content-type: application/x-www-form-urlencoded",
                "x-rapidapi-host: google-translate1.p.rapidapi.com",
                "x-rapidapi-key: 524aa1a476mshfa31a85db63346fp155ff9jsn7eb3f0035930"
            ];

            $params = [
                "q"      => $text, //urlencode(utf8_encode()),
                "source" => "en",
                "target" => "hy"
            ];

            $response = $this->client->post("https://google-translate1.p.rapidapi.com/language/translate/v2", [
                    "headers" => $headers,
                    "allow_redirects" => [
                        "max" => 10,
                        "referer" => true,
                    ],
                    "timeout" => 30,
//                    "version" => 1.1,
                    "json" => $params,
                    'curl' => [CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1],
                    "debug" => false,
            ]);

            return json_encode($response->getBody());

        } catch (ClientException $e) {

            return  "Operation filed: ". json_decode($e->getResponse()->getBody()->getContents())->message; //"Operation filed: " . $e->getResponse()->getStatusCode();

        }*/

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://language-translator1.p.rapidapi.com/translate",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\r
    \"output_language\": \"hy\",\r
    \"text\": \"{$text}\"\r
}",
            CURLOPT_HTTPHEADER => [
                "content-type: application/json",
                "output_language: hy",
                "text: {$text}",
                "x-rapidapi-host: language-translator1.p.rapidapi.com",
                "X-RapidAPI-Key: c4dc6cefadmsh67992b7bd8aca17p123880jsn7b02e15315c3"
            ],
        ]);


        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "";//"Operation filed: " . $err;
        } else {
            return $response;
        }

    }

    /**
     *
     *
     * @param string $target
     * @return array
     */
    public function translateToHy(string $target): array {
        $info = [];
        $fields = $this->isValid($target);
        $hyText = $this->getHyText($target);
        if(is_null($fields["error"]) && !empty($hyText)) {
            $info["success"] = $fields["success"];
            $info["translation"] = html_entity_decode(preg_replace("/U\+([0-9A-F]{4})/", "&#x\\1;", json_decode($hyText)->text), ENT_NOQUOTES, 'UTF-8');
            $info["error"] = $fields["error"];
            return $info;
        }
        $info["error"] = $hyText;
        return is_null($fields["error"]) ? $info : $fields;
    }


    /**
     *
     *
     * @param string $target
     * @return array
     */
    public function isValid(string $target): array {

        $data = [
            "translation" => "",
        ];

        $is_valid = is_numeric($target);

        if($is_valid) {
            $data["error"] = "Target is not a string";
            $data["success"] = false;
            return $data;
        }

        $data["error"] = NULL;
        $data["success"] = true;
        return $data;

    }

    /**
     *
     * @return array
     */
    public function getAll(): array {
       return $this->translate->get()->toArray();
    }


}
