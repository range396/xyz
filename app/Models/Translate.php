<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Translate extends Model
{
    //
    protected $fillable = ['success', 'error', 'translation'];

    protected $hidden = [
        'created_at', 'updated_at', 'id'
    ];
}
