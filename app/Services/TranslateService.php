<?php

namespace App\Services;

use App\Repositories\TranslateRepository as LangRepo;

class TranslateService
{

    /**
     *
     * @var LangRepo $langRepo
     */
    private $langRepo;


    public function __construct(LangRepo $langRepo)
    {
        $this->langRepo = $langRepo;
    }


    /**
     *
     * @param array $target
     * @return array|bool
     */
    public function saving(array $target) {
        if(is_null($target["error"]) && $this->langRepo->store($target))
            return $target;
        return false;
    }

    /**
     *
     *
     * @param array $data
     * @param string $to
     * @return array
     */
    public function translate(string $data, string $to): array {
        if(!empty($to) && $to == "hy") {
            return $this->hy($data);
        }
    }

    /**
     *
     *
     * @param  string $target
     * @return array
     */
    public function hy(string $target): array {
        return $this->langRepo->translateToHy($target);
    }

    /**
     *
     * @return array
     */
    public function all(): array {
       return $this->langRepo->getAll();
    }

}
