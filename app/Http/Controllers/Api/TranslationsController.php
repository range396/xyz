<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\TranslateService as Translate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class TranslationsController extends Controller
{

    private $translation;

    public function __construct(Translate $translation) {
        $this->translation = $translation;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json($this->translation->all());
    }


    /**
     *
     * @param Request $request
     * @return \Illuminate\Http\Response|string
     */
    public function translate(Request $request) {
        if(empty($request->get('targeter'))) {
            return "Notice: Target is empty ( ?target={Text} )";
        }

        return response()->json($this->translation->translate($request->get('targeter'), "hy"), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has("targeter")) {

            $val = $request->get("targeter");
            $translate = $this->translation->translate($val, "hy");
            $ok = $this->translation->saving($translate);
            if(is_array($ok)) {
                return response()->json($translate);
            }
        }
        return response()->json(["success" => false, "translation" => "", "error" => "data not saved"]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
