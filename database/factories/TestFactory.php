<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Translate;
use Faker\Generator as Faker;

$factory->define(Translate::class, function (Faker $faker) {

    $bool = $faker->boolean;
    return [
        "translation" => $bool == true ? $faker->firstName : NULL,
        "success" => $bool,
        "error" => $bool == true ? $faker->text(15) : NULL,
        "created_at" =>$faker->dateTime,
        "updated_at" =>$faker->dateTime,
    ];

});
